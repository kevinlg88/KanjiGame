﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instance = null;


    public LevelManager levelManager;
    public MenuManager menuManager;
    public KanjiDatabase database;
    //[HideInInspector]
    public int nivel = 0;
    //[HideInInspector]
    public int nivelMax = 0;

    [Header("Variaveis de jogo")]
    public List<string> acertos = new List<string>();
    public List<string> tempo = new List<string>();
    public float tempoAcertoSegundos = 0;
    public float tempoErroSegundos = 0;
    public string tempoDeTeste = "";
    public float tempoSegundos = 0;
    public float tempoMostrar = 2;
    public string nome = "no name";
    //public float tempoResponder;

    void Awake()
    {
        if (Instance == null) { Instance = this; }

        else if (Instance != this) { Destroy(gameObject); }
   
        DontDestroyOnLoad(this.gameObject);
        acertos.Clear();
        print("nivel: " + nivel);
        print("nivel MAX: " + nivelMax);

    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Zerar()
    {
        nivel = 0;

        nivelMax = 0;

        acertos = new List<string>();
        tempo = new List<string>();
        tempoAcertoSegundos = 0;
        tempoErroSegundos = 0;
        tempoDeTeste = "";
        tempoSegundos = 0;
        tempoMostrar = 2;
        nome = "no name";
    }
}
