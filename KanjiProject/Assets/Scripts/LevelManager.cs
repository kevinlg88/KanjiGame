﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class LevelManager : MonoBehaviour
{
    //Pop up Fim
    public GameObject fim;
    //Texto tempo
    public Text tempo;
    //Imagem na cena
    public Image imagemMaster;
    //Escolher
    public Image imagemOne;
    public Image imagemTwo;

    //Sprites randomizados
    Sprite numberOne;
    Sprite numberTwo;

    private int currentLevel;
    private int maxLevel;
    private float tempoMostrar;

    //Usuario
    private string nameUser;

    //State
    private bool canAction = false;

    //Time
    private float startTime;
    private float startTimeTest;

    void Awake()
    {
        maxLevel = GameManager.Instance.nivelMax;
        currentLevel = GameManager.Instance.nivel;
        nameUser = GameManager.Instance.nome;
        tempoMostrar = 2;
    }
    void Start()
    {
        StartCoroutine(ShowSprite());
        currentLevel = 0;
        startTime = Time.time;
        startTimeTest = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentLevel < maxLevel)
        {
            float tempoTeste = Time.time - startTimeTest;

            string minutesTeste = ((int)tempoTeste / 60).ToString();
            float tempoSegundos = tempoTeste;
            string secondsTeste = (tempoTeste % 60).ToString("f3");

            string tempoTesteString = minutesTeste + "m :" + secondsTeste + "s";

            if (canAction)
            {
                float t = Time.time - startTime;

                string minutes = ((int)t / 60).ToString();
                float minutesSeconds = t;
                string seconds = (t % 60).ToString("f3");

                tempo.text = minutes + "m :" + seconds + "s";
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    GameManager.Instance.tempo.Add(tempo.text);
                    tempo.text = "";
                    if (imagemOne.sprite == numberOne)
                    {
                        GameManager.Instance.acertos.Add("Acertou");
                        GameManager.Instance.tempoAcertoSegundos += minutesSeconds;
                    }

                    else
                    {
                        GameManager.Instance.acertos.Add("Errou");
                        GameManager.Instance.tempoErroSegundos += minutesSeconds;
                    }
                    currentLevel += 1;
                    GameManager.Instance.nivel += 1;
                    canAction = false;
                    imagemOne.gameObject.SetActive(false);
                    imagemTwo.gameObject.SetActive(false);
                    if (currentLevel >= maxLevel)
                    {
                        fim.SetActive(true);
                        GameManager.Instance.tempoDeTeste = tempoTesteString;
                        GameManager.Instance.tempoSegundos = tempoSegundos;
                        CriarRelatorio();
                    }
                    else
                    {
                        StartCoroutine(ShowSprite());
                    }

                }

                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    GameManager.Instance.tempo.Add(tempo.text);
                    tempo.text = "";
                    if (imagemTwo.sprite == numberOne)
                    {
                        GameManager.Instance.acertos.Add("Acertou");
                        GameManager.Instance.tempoAcertoSegundos += minutesSeconds;
                    }

                    else
                    {
                        GameManager.Instance.acertos.Add("Errou");
                        GameManager.Instance.tempoErroSegundos += minutesSeconds;
                    }
                    currentLevel += 1;
                    GameManager.Instance.nivel += 1;
                    canAction = false;
                    imagemOne.gameObject.SetActive(false);
                    imagemTwo.gameObject.SetActive(false);
                    if (currentLevel >= maxLevel)
                    {
                        fim.SetActive(true);
                        GameManager.Instance.tempoDeTeste = tempoTesteString;
                        GameManager.Instance.tempoSegundos = tempoSegundos;
                        CriarRelatorio();
                    }
                    else
                    {
                        StartCoroutine(ShowSprite());
                    }
                }
            }
        }
    }

    private void RandomizarSprite()
    {
        int tamanhoData = GameManager.Instance.database.spritesList.Count;
        int randomNumberOne = Random.Range(0, tamanhoData);
        int randomNumberTwo = Random.Range(0, tamanhoData);
        while (randomNumberTwo == randomNumberOne)
        {
            randomNumberTwo = Random.Range(0, tamanhoData);
        }
        numberOne = GameManager.Instance.database.spritesList[randomNumberOne];
        numberTwo = GameManager.Instance.database.spritesList[randomNumberTwo];
    }

    IEnumerator ShowSprite()
    {
        RandomizarSprite();
        int random = Random.Range(1, 3);
        imagemMaster.gameObject.SetActive(true);
        imagemMaster.sprite = numberOne;
        yield return new WaitForSeconds(tempoMostrar);
        //imagemMaster.sprite = numberTwo;
        //yield return new WaitForSeconds(tempoMostrar);
        imagemMaster.gameObject.SetActive(false);

        imagemOne.gameObject.SetActive(true);
        imagemTwo.gameObject.SetActive(true);
        if (random == 1)
        {
            imagemOne.sprite = numberOne;
            imagemTwo.sprite = numberTwo;
        }
        else
        {
            imagemOne.sprite = numberTwo;
            imagemTwo.sprite = numberOne;
        }
        canAction = true;
        startTime = Time.time;

    }

    public void backToMenuBttn()
    {
        GameManager.Instance.Zerar();
        SceneManager.LoadScene("Menu");
    }


    private void CriarRelatorio()
    {

        string pathDir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/" + nameUser;
        string pathFile = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/" + nameUser + "/Relatorio.txt";

        if (!Directory.Exists(pathDir))
        {
            Directory.CreateDirectory(pathDir);
        }

        if (!File.Exists(pathFile))
        {
            File.WriteAllText(pathFile,
                "Data do experimento: " +
                System.DateTime.Now + System.Environment.NewLine + System.Environment.NewLine +
                "Quantidade de tentativas: " +
                maxLevel + System.Environment.NewLine + System.Environment.NewLine);

            int i;
            int numeroDeAcertos = 0;
            int numeroDeErros = 0;
            float taxaDeAcertos = 0;
            float taxaDeErros = 0;
            float indiceDeDiscriminacao = 0;
            string content = "";
            for (i = 0; i < maxLevel; i++)
            {
                content += "### Tentativa: " + (i+1) + "###" + System.Environment.NewLine + System.Environment.NewLine +
                    GameManager.Instance.acertos[i] + System.Environment.NewLine + System.Environment.NewLine +
                    "Tempo de resposta: " + GameManager.Instance.tempo[i] + System.Environment.NewLine + System.Environment.NewLine;
                if (GameManager.Instance.acertos[i] == "Acertou")
                {
                    numeroDeAcertos += 1;
                }
                else
                {
                    numeroDeErros += 1;
                }
            }
            if (GameManager.Instance.tempoAcertoSegundos != 0)
            {
                taxaDeAcertos = (numeroDeAcertos / GameManager.Instance.tempoAcertoSegundos) * 60;
            }
            if (GameManager.Instance.tempoErroSegundos != 0)
            {
                taxaDeErros = (numeroDeErros / GameManager.Instance.tempoErroSegundos) * 60;
            }
            indiceDeDiscriminacao = (taxaDeAcertos / (taxaDeAcertos + taxaDeErros)) * 100;
            File.AppendAllText(pathFile, content +
                "Tempo de teste: " + GameManager.Instance.tempoDeTeste + System.Environment.NewLine +
                "Indice de discriminação: " + indiceDeDiscriminacao + "%" + System.Environment.NewLine);
        }

        else
        {
            File.AppendAllText(pathFile, System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine + 
                System.Environment.NewLine + " ###########################" +
                "Data do experimento: " +
                System.DateTime.Now + System.Environment.NewLine + System.Environment.NewLine +
                "Quantidade de tentativas: " +
                maxLevel + System.Environment.NewLine + System.Environment.NewLine);

            int i;
            int numeroDeAcertos = 0;
            int numeroDeErros = 0;
            float taxaDeAcertos = 0;
            float taxaDeErros = 0;
            float indiceDeDiscriminacao = 0;
            string content = "";
            for (i = 0; i < maxLevel; i++)
            {
                content += "### Tentativa: " + (i + 1) + "###" + System.Environment.NewLine + System.Environment.NewLine +
                    GameManager.Instance.acertos[i] + System.Environment.NewLine + System.Environment.NewLine +
                    "Tempo de resposta: " + GameManager.Instance.tempo[i] + System.Environment.NewLine + System.Environment.NewLine;
                if (GameManager.Instance.acertos[i] == "Acertou")
                {
                    numeroDeAcertos += 1;
                }
                else
                {
                    numeroDeErros += 1;
                }
            }
            if (GameManager.Instance.tempoAcertoSegundos != 0)
            {
                taxaDeAcertos = (numeroDeAcertos / GameManager.Instance.tempoAcertoSegundos) * 60;
            }
            if (GameManager.Instance.tempoErroSegundos != 0)
            {
                taxaDeErros = (numeroDeErros / GameManager.Instance.tempoErroSegundos) * 60;
            }
            indiceDeDiscriminacao = (taxaDeAcertos / (taxaDeAcertos + taxaDeErros)) * 100;
            File.AppendAllText(pathFile, content +
                "Tempo de teste: " + GameManager.Instance.tempoDeTeste + System.Environment.NewLine +
                "Indice de discriminação: " + indiceDeDiscriminacao +"%" + System.Environment.NewLine);
        }
    }
}

        
