﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public GameObject instrucao;
    public GameObject warningNiveis;
    public InputField quantidadeNiveis, nomeUser;

    void Awake()
    {
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Clicar no botão de comecar teste
    public void StartTestBttn()
    {
        if (quantidadeNiveis.text != "" && quantidadeNiveis.text != "0" && nomeUser.text != "")
        {
            GameManager.Instance.nivelMax = int.Parse(quantidadeNiveis.text);
            GameManager.Instance.nome = nomeUser.text;
            print("" + GameManager.Instance.nivelMax);
            instrucao.SetActive(true);
        }
        else
        {
            warningNiveis.SetActive(true);
        }
    }

    //Clicar no botão de fechar warning
    public void CloseWarningNiveisBttn()
    {
        warningNiveis.SetActive(false);
    }

    //Clicar no botão de avançar instruções
    public void AvancarIntrucao()
    {
        SceneManager.LoadScene("Jogo");
    }
}
